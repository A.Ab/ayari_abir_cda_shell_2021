#!/bin/bash
function fact(){
VAR=$1
FACT=1
while [ $VAR -gt 0 ]
do
	FACT=$(( $FACT*$VAR ))
	VAR=$(( $VAR - 1 ))
done
echo "Le factorielle de :$1 est $FACT"
}
fact $1

